require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const cors = require('cors')
// const config = require('./config/config')
const routes = require('./server/api/routes/index')
// create express app

const app = express()

app.use(helmet()) // secure app by setting HTPP headers
app.use(cors()) // enable Cross Origin Resource Sharing
app.use(bodyParser.urlencoded({ extended: true })) // parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.json()) // parse requests of content-type - application/json

/** MOUNT ALL SERVICES ROUTES */
// `/${config.version}`,
app.use(routes)

// listen for requests
app.listen(8002, () => {
    console.log("Server is listening on port 8002")
})