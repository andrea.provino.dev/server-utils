const express = require('express')
const stripe_routes = require('../V1/stripe/stripe.routes')
const s3_routes = require('../V1/s3/s3.routes')
const router = express.Router()

router.use(stripe_routes)
router.use(s3_routes)

module.exports = router