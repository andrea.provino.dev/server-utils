const fs = require('fs')
const path = require('path')
var AWS = require("aws-sdk")
const multer = require('multer')
const multerS3 = require('multer-s3')
const s3 = new AWS.S3()


const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'api-monolith',
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: function (req, file, cb) {
            console.log(file)
            cb(null, file.originalname + Date.now())
        }
    })
})

const uploadSingle = (req, res, next) => {
    upload.single("file")
    next()
}
const uploadIt = (req, res) => {
    // filename
    try {
        console.log(req)
        if (req.body.file === undefined) {
            return res.status(400).send({
                "message": "no file found"
            })
        }
        const file = req.body.file
        var params = {
            Bucket: 'api-monolith',
            Key: file.originalname,
            Body: file.buffer,
            ContentType: file.mimetype,
            ACL: "public-read"
        }
        s3.upload(params, function (err, data) {
            if (err) {
                res.status(500).json({ error: true, Message: err })
            } else {
                res.send({ data })

            }
        })
        // upload.single('image')
        /** 
        var uploadParams = {
            Bucket: 'api-monolith',
            Key: '',
            Body: '',
            ACL: 'public-read'
        }
        var file = "face.jpeg"
        var fileStream = fs.createReadStream(file)
        fileStream.on('error', function (err) {
            return res.status(500).send({
                "message": err
            })
        })
        uploadParams.Body = fileStream
        uploadParams.Key = path.basename(file)
        s3.upload(uploadParams, function (err, data) {
            if (err) {
                return res.status(500).send({
                    "message": err
                })
            } if (data) {
                return res.status(200).send({
                    "message": `File uploaded successfully at ${data.Location}`,
                    "data": data
                })
            }
        })
        */
    } catch (err) {
        res.status(500).send({
            e: err
        })
    }

}
const uploadFile = [uploadSingle, uploadIt]
module.exports = {
    uploadFile
}


