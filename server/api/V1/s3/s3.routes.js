const express = require('express')
const router = express.Router()
const ctrl = require('./s3.controller')
var multer = require('multer')
const multerS3 = require('multer-s3')
var AWS = require("aws-sdk")
const s3 = new AWS.S3()
const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'api-monolith',
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: function (req, file, cb) {
            if (!req["uploadedFile"]) {
                req["uploadedFile"] = []
            }
            console.log(file)
            const fileName = (Date.now() + "-" + file.originalname)
            req["uploadedFile"].push(fileName)
            cb(null, fileName)
        }
    })
})

router.route("/uploadFile")
    .post(upload.array("file"), function (req, res) {
        res.send({
            message: 'Successfully uploaded  file!',
            file: {
                baseUrl: "https://api-monolith.s3-eu-west-1.amazonaws.com/",
                name: req.uploadedFile
            }
        })
    })
    .get((req, res) => res.status(200).send("work"))

module.exports = router