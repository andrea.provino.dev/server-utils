const express = require('express')
const router = express.Router()
const ctrl = require('./stripe.controller')

router.route("/foo")
    .get(ctrl.foo)

router.route("/client")
    .get(ctrl.handleClientSide)


router.route("/charge")
    .post(ctrl.charge)

router.route("/checkout")
    .post(ctrl.checkOut)

module.exports = router
