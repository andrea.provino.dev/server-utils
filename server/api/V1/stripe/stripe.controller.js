const { config } = require("../../../../config/config")
const stripe = require('stripe')(config.stripe.private_key)

const foo = (req, res) => {
    res.status(200).send({
        message: "Foo ready"
    })
}
const handleClientSide = async (req, res) => {
    console.log("reached")
    res.sendFile("client.html", { root: __dirname })
}
const charge = async (req, res) => {
    // get token
    const token = req.body.stripeToken
    // save stripe token to the user who made the request 
    // handle logged in user and not logged in
    try {
        const product = {
            amount: 99,
            currency: 'eur',
            description: 'Example charge',
            source: token,
            statement_descriptor: "Test"
        }
        const charge = await stripe.charges.create(product)
        /**
         * Ensure the email you’re expecting is for a live transaction. 
         * Stripe only sends email receipts for payments that are made 
         * with your live API key or through your live Dashboard. If you
         * are expecting an email for a test transaction, one will not be sent
         * https://support.stripe.com/questions/not-receiving-email-sent-from-stripe
         */
        const receipt = {
            amount_payed: product.amount,
            currency: "eur",
            currency_symbol: "€",
            description: product.description,
            card_token: token,
            online_doc: charge.receipt_url,
            full_res: charge,
            receipt_email: 'andreprovino@gmail.com'
        }

        res.status(200).send({
            message: "Payment Done",
            receipt: receipt,
            charge: charge,
            invoice: {}
        })
    } catch (e) {
        res.status(500).send(e)
    }



}
const checkOut = async (req, res) => {
    try {
        stripe.checkout.sessions.create({
            payment_method_types: ['card'],
            billing_address_collection: "auto", // or auto, collect address data
            client_reference_id: "aws-sub_1%6&1$",
            // customer: "aws-sub_1%6&1$", //have to be registered on accoutn creation
            line_items: [{
                name: 'T-shirt',
                description: 'Comfortable cotton t-shirt',
                images: ['https://cdn.pixabay.com/photo/2019/09/13/15/13/chicken-4474176_1280.jpg'],
                amount: 50,
                currency: 'eur',
                quantity: 2
            },
            {
                name: 'T-shirt',
                description: 'Tree',
                images: ['https://cdn.pixabay.com/photo/2019/09/28/20/21/trees-4511721_1280.jpg'],
                amount: 52,
                currency: 'eur',
                quantity: 1
            }],
            success_url: 'http://localhost:3000/success?session_id={CHECKOUT_SESSION_ID}',
            cancel_url: 'http://localhost:3000/cancel'
        }).then(session => {
            // 
            res.status(200).send(session)
        }).catch(err => {
            const code = err.raw.code
            console.log(code)
            /*
            switch (code) {
                case 'amount_too_small':
                    const payload = {
                        code: err.raw.code,
                        description: err.raw.message
                    }
                    return res.status(400).send(payload)
            }
            */
            res.status(err.raw.statusCode).send(err)
        })
    } catch (err) {
        res.status(500).send(err)
    }

}
module.exports = {
    foo,
    charge,
    handleClientSide,
    checkOut
}