
const config = {
    stripe: {
        public_key: process.env.STRIPE_PUBLIC_KEY,
        private_key: process.env.STRIPE_SECRET_KEY,
        webhook_sceret: process.env.STRIPE_WEBHOOK_SECRET
    },
    version: "v1"
}

module.exports = {
    config
}